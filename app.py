#!/usr/bin/python3.7

import csv
import smtplib
import sys
from email.mime.text import MIMEText
import random
import os
import re


def usage(exit_value: int):
    print("USAGE")
    print("\t./app.py [Path] [Mail address] [Mail mdp] [SMTP Parameter]")
    print("DESCRIPTION")
    print("\tPath\tPath to CSV folder")
    print("\tMail address\tYour address mail")
    print("\tMail mdp\tYour mail password")
    print("\tSMTP parameters\tsmtp.example.com:port (Get it on internet)")
    exit(exit_value)


def send_mail(receiver: str, mail_object: str, mail_text: str):
    message = MIMEText(mail_text)
    message['Subject'] = mail_object
    message['From'] = sys.argv[2]
    message['To'] = receiver
    server = smtplib.SMTP(sys.argv[4])
    server.starttls()
    server.login(sys.argv[2], sys.argv[3])
    server.send_message(message)
    server.quit()


def get_csv_content(csv_paths: list) -> list:
    student_list = []

    for csvPath in csv_paths:
        file = open(csvPath, "r")
        csv_file = csv.reader(file)
        for row in csv_file:
            if row[0]:
                student_list.append(row[0])
    return student_list


def main(student_list: list):
    message = ""
    mail_object = ""
    shuffle_list = student_list.copy()

    random.shuffle(shuffle_list)

    with open("mailText.txt", "r") as mailText:
        for line in mailText:
            split_line = line.split(":")
            if split_line[0] is not None and split_line[0] == "Object":
                mail_object = split_line[1]
                continue
            message += line

    with open("log.txt", "a") as file:
        for i in range(len(shuffle_list)):
            file.write("Sender = " + str(shuffle_list[i]) + " | " + "Receiver = " + str(
                shuffle_list[(i + 1) % len(shuffle_list)]) + "\n")
            send_mail(shuffle_list[i], mail_object,
                      message + shuffle_list[(i + 1) % len(shuffle_list)])
        file.write("-" * 7 + "END" + "-" * 7 + "\n")


if __name__ == "__main__":
    nb_args = len(sys.argv)

    if nb_args != 5:
        usage(84)
    elif nb_args == 2 and sys.argv[1] == "-h":
        usage(0)
    else:
        list_csv = []
        entries = os.listdir(sys.argv[1])
        for entry in entries:
            if re.findall("\.csv$", entry) is not None:
                list_csv.append(sys.argv[1] + "/" + entry)
        main(get_csv_content(list_csv))
