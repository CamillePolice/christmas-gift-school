# Subject
This is an awesome random generator christmas list. Give him CSV file with people you want, it will
send an email with the person you've to buy a gift

## Usage
```python
                                DESCRIPTION
./app.py [Path] [Mail address] [Mail mdp] [SMTP Parameter]
Path
    Path to CSV folder
Mail address
    Your address mail
Mail mdp
    Your mail password
SMTP parameters
    smtp.example.com:port (Get it on internet)
```
You have to write your mail in the mailText.txt file.
 
## License
[GNU Licence](https://www.gnu.org/licenses/gpl-3.0.fr.html)